# tinyunix


# Introduction 

Antic memstick, with BSD and Linux. 


# Content 


Partition 1, Modern Grub, Grub2 
with plpbt., slackware to ramdisk

partition 1, working netbsd 7. with netbsd kernel and netbsd-INSTALL.gz (to ramdisk, with USB driver).

partition 2, the Sarge Debian 3.1, with an improved Kernel 3.x.



# Installation 

Copy using zcat the memstick onto /dev/sdX

If you would like to install it to /dev/wd0d, then, use:

dd if=/dev/sd0d of=/dev/wd0d

Once done, edit fstab.
Change sd0a to wd0a. 






# Small size

The systems are working on most older computer of pentium and earlier generations.

The modern netbsd is installed with sysinst. It allows to deploy the system on other machines. 






